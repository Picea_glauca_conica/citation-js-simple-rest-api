const fs = require('fs')

const Cite = require('citation-js')
const marked = require('marked')

const express = require('express')
const app = express()
const port = process.env.PORT || 3000
const apiPrefix = process.env.API_ROOT || ''

let readme = marked(fs.readFileSync(__dirname + '/README.md', 'utf8'))

function parseRequest ({params, query}) {
  let fragments = params[0].split('/')
  let format = 'bibliography'

  if (Cite.plugins.output.has(fragments[fragments.length - 1])) {
    format = fragments.pop()
  }

  let data = fragments.join('/')

  let config = {}
  let options = {}

  for (let param in query) {
    if (param.startsWith('_')) {
      config[param.slice(1)] = query[param]
    } else {
      options[param] = query[param]
    }
  }

  return {data, format, config, options}
}

app
  .get('/', (_, res) => res.send(readme))
  .get(apiPrefix + '/*', (req, res) => {
    let {data, format, options, config} = parseRequest(req)
    let cite = new Cite(data)
    let output = cite.format(format, options)
    res.send(output)
  })

app.listen(port, () => console.log(`App listening on port ${port}...`))
