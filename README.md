This is a small API to test Docker.

## API syntax

    /$DATA[/$FORMAT]

Where `$DATA` is input data and `$FORMAT` is output format. Additional output options can be given as query parameters. For documentation on output format and options, see the [Citation.js docs](https://citation.js.org/api/tutorial-cite_output.html).

    /

Goes to the docs, this page.

## Example

  * `/10.1021/ja01577a030/bibtex?format=json`: Metadata of doi:10.1021/ja01577a030 as BibTeX in JSON form
  * `/Q30000000/bibliography?template=apa`: APA bibliography of wikidata:Q30000000
