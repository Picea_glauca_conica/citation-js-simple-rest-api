FROM node:10
WORKDIR /app
COPY . /app
EXPOSE 80
ENV PORT 80
ENV API_ROOT "/api/v1"
CMD ["npm", "start"]
